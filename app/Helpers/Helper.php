<?php
/**
 * Created by PhpStorm.
 * User: jose
 * Date: 20-02-18
 * Time: 11:09
 */

namespace App\Helpers;


class Helper
{
    /**
     * @param $success
     * @param null|mixed $content
     * @return \Illuminate\Http\JsonResponse
     */
    public static function jsonResponse($success, $content = null)
    {
        $response = ['success' => $success,];
        if ($success && $content !== null) {
            unset($response['message']);
            $response['data'] = $content;
        }

        if ($success === false && $content !== null) {
            unset($response['data']);
            $response['message'] = $content;
        }

        return response()->json($response);
    }
}