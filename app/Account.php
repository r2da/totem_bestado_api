<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Account extends Model
{
    protected $fillable = ['account_number', 'status', 'service', 'amount', 'rut', 'pass', 'coord_1', 'coord_2', 'coord_3', 'val_coord_1', 'val_coord_2', 'val_coord_3', 'nombre', 'email'];
}
